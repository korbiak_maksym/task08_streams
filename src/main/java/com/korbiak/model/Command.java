package com.korbiak.model;

public interface Command {
    String getString(String str);
}
