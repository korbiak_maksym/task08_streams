package com.korbiak.model;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex4 {

    String lines;
    Stream<String> stringStream;

    public Ex4(String lines) {
        this.lines = lines;
        stringStream = Stream.of(lines);
    }

    public int getNumberOfUniqueWords() {
        return (int) stringStream
                .distinct()
                .count();
    }

    public List<String> getSortedList() {
        return stringStream
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> getStatistic() {
        return stringStream
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    public Map<String, Long> getSymbolStatistic() {
        return stringStream
                .flatMap(e -> Stream.of(e.split("")))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

}
