package com.korbiak.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Ex3 {
    private List<Integer> list;

    public Ex3(int count) {
        list = new ArrayList<>(count);
        int number = 0;
        for (int i = 0; i < count; i++) {
            number = (int) ((Math.random() * ((100) + 1)) + 0);
            this.list.add(number);
        }

    }

    public int getAver() {
        Stream<Integer> streamList = list.stream();
        int aver = getSum()/(int) streamList.count();
        return aver;
    }

    public int getMax() {
        Stream<Integer> streamList = list.stream();
        return streamList.max(Integer::compareTo).get();
    }

    public int getMin() {
        Stream<Integer> streamList = list.stream();
        return streamList.min(Integer::compareTo).get();
    }

    public int getSum() {
        Stream<Integer> streamList = list.stream();
        return streamList.reduce((sum, elem) -> sum += elem).get();
    }

    public int getBigger() {
        Stream<Integer> streamList = list.stream();
        return streamList.filter((elem) -> elem > getAver()).reduce((sum, elem) -> sum += elem).get();
    }

}
