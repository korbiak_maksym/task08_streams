package com.korbiak.model;

import java.util.LinkedList;
import java.util.List;

public class BusinessLogic {

    public BusinessLogic() {
        initCom();
    }

    //------------------ex1--------------------
    public int getAver(int a, int b, int c) {
        FunInterfaceOfEx1 funInterface = (a1, b1, c1) -> (a1 + b1 + c1) / 3;
        return funInterface.getInt(a, b, c);
    }

    public int getMax(int a, int b, int c) {
        FunInterfaceOfEx1 funInterface = (a1, b1, c1) -> {
            int answer = a1;
            if (b1 > answer) {
                answer = b1;
            }
            if (c1 > answer) {
                answer = c1;
            }
            return answer;
        };
        return funInterface.getInt(a, b, c);
    }

    //------------------ex2--------------------

    private List<Command> commands = new LinkedList<>();

    public String getCommand(String str1, String str2) {
        String answer = null;
        if (str1.equals("anonymous")) {
            answer = commands.get(0).getString(str2);
        } else if (str1.equals("lambda")) {
            answer = commands.get(1).getString(str2);
        } else if (str1.equals("reference")) {
            answer = commands.get(2).getString(str2);
        } else if (str1.equals("object")) {
            answer = commands.get(3).getString(str2);
        }
        return answer;
    }

    private void initCom() {
        commands.add((str) -> ("Lambda message:" + str));
        commands.add(new Command() {
            @Override
            public String getString(String str) {
                return "Anonymous message:" + str;
            }
        });
        commands.add(this::getString);
        commands.add(new CommandImpl());
    }

    private String getString(String string) {
        return "Reference message:" + string;
    }

    //------------------ex3--------------------
    Ex3 ex3 = new Ex3(10);

    public int getAver() {
        return ex3.getAver();
    }

    public int getMax() {
        return ex3.getMax();
    }

    public int getMin() {
        return ex3.getMin();
    }

    public int getSum() {
        return ex3.getSum();
    }

    public int getBigger() {
        return ex3.getBigger();
    }


}
