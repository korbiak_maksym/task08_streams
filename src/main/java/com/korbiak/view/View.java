package com.korbiak.view;

import com.korbiak.Application;
import com.korbiak.controller.Controller;
import com.korbiak.model.Ex3;
import com.korbiak.model.Ex4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
    private Controller controller;
    private Logger logger;

    public View() {
        this.controller = new Controller();
        logger = LogManager.getLogger(Application.class);
    }

    public void show() {
        //getEx1();
        //getEx2();
        //getEx3();
       
    }

    private void getEx1() {
        logger.trace(controller.getAver(4, 2, 6));
        logger.trace(controller.getMax(4, 2, 6));
    }

    private void getEx2() {
        logger.trace(controller.getCommand("anonymous", "first"));
        logger.trace(controller.getCommand("lambda", "second"));
        logger.trace(controller.getCommand("reference", "third"));
        logger.trace(controller.getCommand("object", "four"));
    }

    private void getEx3() {
        logger.trace("Aver:" + controller.getAver());
        logger.trace("Max:" + controller.getMax());
        logger.trace("Bigger:" + controller.getBigger());
        logger.trace("Min:" + controller.getMin());
        logger.trace("Sum:" + controller.getSum());
    }
}
