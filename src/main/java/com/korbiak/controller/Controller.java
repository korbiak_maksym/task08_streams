package com.korbiak.controller;

import com.korbiak.model.BusinessLogic;

public class Controller {
    private BusinessLogic businessLogic;

    public Controller() {
        this.businessLogic = new BusinessLogic();
    }

    //------------------ex1--------------------
    public int getAver(int a, int b, int c) {
        return businessLogic.getAver(a, b, c);
    }

    public int getMax(int a, int b, int c) {
        return businessLogic.getMax(a, b, c);
    }

    //------------------ex2--------------------

    public String getCommand(String str1, String str2){
        return businessLogic.getCommand(str1, str2);
    }

    //------------------ex3--------------------

    public int getAver() {
        return businessLogic.getAver();
    }

    public int getMax() {
        return businessLogic.getMax();
    }

    public int getMin() {
        return businessLogic.getMin();
    }

    public int getSum() {
        return businessLogic.getSum();
    }

    public int getBigger() {
        return businessLogic.getBigger();
    }

}
